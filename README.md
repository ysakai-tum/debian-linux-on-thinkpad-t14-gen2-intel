# Linux (Debian 11 bullseye) on a ThinkPad T14 gen. 2 Intel

This is my installation/set-up note for my business laptop Lenovo ThinkPad Gen2 Intel with Debian 11. 

# Hardware specs


## Shipped:

- Host: 20W1FH4N7163FL ThinkPad T14 Gen 2
- Resolution: 1920x1080 
- CPU: 11th Gen Intel Core i7 1165G7 (2.8 GHz)
- GPU: Nvidia TU117M [GeForce MX450], 2 GB VRAM, Compute Capability 7.5 
- Memory: 16 GB
- SSD: 512 GB
- Security: finger print sensor
- WiFi: Intel Corporation Wi-Fi 6 AX201 [8086:a0f0] (rev 20)
- Audio: Intel Corporation Tiger Lake-LP Smart Sound Technology Audio Controller (rev 20) (prog-if 80)

## Custom:

- additional 16 GB RAM (3200 MHz SO-DIMM)

# BIOS

- shipped: N34ET47W (1.47) + TEC (1.38) 
- updated: N34ET49W (1.49) + TEC (1.40)

# Distribution

Debian 11 bullseye 

# Installation

Default

# GPU

I installed NVIDIA proprietary driver by following the instruction from <https://wiki.debian.org/NvidiaGraphicsDrivers#Debian_11_.22Bullseye.22>

- nvidia-driver version: 470.103.01 (via backport) 

''
apt install linux-headers-amd64
apt install nvidia-detect
apt install -t bullseye-backports nvidia-driver firmware-misc-nonfree
apt install nvidia-xconfig
''

# Problems and Fixes

Below are some problems out-of-the-box:

## 1. BIOS update problem

Issue: <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=968997>

Cause: secure boot being enabled (see below)


### Attempt 1
First I downloaded n34ul15w.* from <https://pcsupport.lenovo.com/py/en/downloads/ds548904-bios-update-utility-bootable-cd-for-windows-10-64-bit-thinkpad-p14s-gen-2-p15s-gen-2-t14-gen-2-t15-gen-2> and tried the following on the terminal according to the README:
''
fwupdmgr refresh
fwupdmgr install xxx.cab
''

which completes without returning an error, but I still see the old UEFI BIOS version after reboot.

### Attempt 2 
Then I tried simply double-click the CAB-file which executes normally, but then the BIOS version stays the same.

### Attempt 3 

''
fwupdmgr refresh --force
fwupdmgr get-devices 
fwupdmgr get-updates 
fwupdmgr update
''

Still the same issue that successfully updates BIOS firmware, but no effect on reboot

### Attempt 4
Same as Attempt 3, but by disabling secure boot -> success!!

## 2. WiFi not detected

Cause: driver not installed

Solution: install firmware-iwlwifi <https://wiki.debian.org/iwlwifi>

## 3. display freeze randomly

Issue: display freeze randomly at an irregular interval and I have to hard shut down by long pressing of power button

<https://unix.stackexchange.com/questions/625668/thinkpad-e14-gen-2-amd-display-freeze>
<https://bbs.archlinux.org/viewtopic.php?id=262646>

Cause: ??
Solution: installing NVIDIA driver somehow fixed the problem (TBC)

## 4. MATLAB installer not running
see <https://www.mathworks.com/matlabcentral/answers/1464434-why-is-the-linux-matlab-install-script-not-opening-the-installer-window>

## 5. NVIDIA drivers installed, but not used for OpenGL

Solution: configure NVIDIA Optimus via Bumblebee

''
sudo apt install bumblebee-nvidia primus-nvidia primus-vk-nvidia
adduser $USER bumblebee
primusrun glxgears -info
''

## 6. No audio hardware detected

Solution: 

''
apt-get install firmware-sof-signed firmware-intel-sound
''
